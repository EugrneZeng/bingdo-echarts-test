import React from "react";
import ReactEcharts from "echarts-for-react";
import echarts from "echarts/lib/echarts";
import "echarts/lib/chart/line";
import "echarts/lib/chart/bar";
import "echarts/lib/chart/pie";
import "echarts/lib/chart/boxplot";
import "echarts/lib/component/tooltip";
import "echarts/lib/component/title";
import { useCharts } from "../contexts/ChartsContext";
import { lineChartProssessor, barChartProssessor, pieChartProssessor } from "../utils/chartProcessors";
import { chartOptions, chartTypes } from "../utils/constants";

const chartWrapperStyle = {
  display: "flex",
  alignItems: "flex-start",
  flexWrap: "wrap"
};
const chartContainerStyle = {
  padding: 10,
  width: "45%"
};
const handleLineChart = (chart, data) => ({
  ...chart,
  series: chart.series.map(serie => ({ ...serie, data: data[serie.name] }))
});

const handleBarChart = (chart, data) => ({
  ...chart,
  series: Object.keys(data).map(key => ({
    ...chart.series[0],
    name: key,
    data: data[key]
  }))
});

const handlePieChart = (chart, data) => ({
  ...chart,
  legend: {
    ...chart.legend,
    data: Object.keys(data)
  },
  series: [
    {
      ...chart.series[0],
      data: Object.keys(data).map(key => ({
        name: key,
        value: data[key]
      }))
    }
  ]
})

const chartHandler = chartType => {
  const chartOption = chartOptions[chartType];
  switch (chartType) {
    case chartTypes.LINECHART:
      return chartData => handleLineChart({ ...chartOption }, lineChartProssessor(chartData));
    case chartTypes.PIECHART:
      return chartData => handlePieChart({ ...chartOption }, pieChartProssessor(chartData));
    case chartTypes.BARCHART:
      return chartData => handleBarChart({ ...chartOption }, barChartProssessor(chartData));
    default:
      return chartOption;
  }
}

const ChartArea = () => {
  const { existingCharts, chartData } = useCharts();
  if (existingCharts.length === 0) return null;
  return (
    <div style={chartWrapperStyle}>
      {existingCharts.map(
        key =>
          (
            <div style={chartContainerStyle}
              key={key}>
              <ReactEcharts
                echarts={echarts}
                option={chartHandler(key)(chartData)}
                notMerge={true}
                lazyUpdate={true}
              /></div>
          )
      )}
    </div>
  );
};
export default ChartArea;
