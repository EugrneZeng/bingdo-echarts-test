import React from "react";
import { Menu } from "antd";
import DraggableItem from "./DraggableItem";
import { chartTypes } from "../utils/constants";

const { SubMenu } = Menu;

const SideMenu = () => {
  return (
    <Menu
      style={{ width: "100%" }}
      defaultOpenKeys={["charTypes"]}
      mode="inline"
    >
      <SubMenu
        title={
          <span>
            <span>Chart Types</span>
          </span>
        }
        key="charTypes"
      >
        <Menu.Item key="lineChart">
          <DraggableItem type={chartTypes.LINECHART}>Line Chart</DraggableItem>
        </Menu.Item>
        <Menu.Item key="areaChart" disabled>
          <DraggableItem type={chartTypes.AREACHART}>Area Chart</DraggableItem>
        </Menu.Item>
        <Menu.Item key="barChart">
          <DraggableItem type={chartTypes.BARCHART}>Bar Chart</DraggableItem>
        </Menu.Item>
        <Menu.Item key="pieChart">
          <DraggableItem type={chartTypes.PIECHART}>
            Pie Chart
          </DraggableItem>
        </Menu.Item>
      </SubMenu>
    </Menu>
  );
};

export default SideMenu;
