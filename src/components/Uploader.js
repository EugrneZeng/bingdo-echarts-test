import React from "react";
import { useDataSource } from "../contexts/DataSourceContext";
import { Icon, Upload, Button, message } from "antd";

const Uploader = () => {
  const { convertFileJson } = useDataSource();
  const props = {
    name: "file",
    multiple: false,
    showUploadList: false,
    customRequest({
      action,
      data,
      file,
      filename,
      headers,
      onError,
      onProgress,
      onSuccess,
      withCredentials
    }) {
      onProgress({ percent: 100 }, file);
      window.setTimeout(() => {
        onSuccess(convertFileJson(file), file);
      }, 100)
      return {
        abort() {
          console.log("upload progress is aborted.");
        }
      };
    },
    onChange(info) {
      if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
      if (info.file.status === "done") {
        message.success(`${info.file.name} file uploaded successfully`);
      }
      return false;
    }
  };
  return (
    <Upload {...props}>
      <Button>
        <Icon type="upload" /> Click to Upload
      </Button>
    </Upload>
  );
};

export default Uploader;
