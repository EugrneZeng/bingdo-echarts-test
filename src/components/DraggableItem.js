import React from "react";
import { Icon } from "antd";
import { useDrag } from "react-dnd";

const DraggableItem = ({ type, children }) => {
  const [{ isDragging }, drag] = useDrag({
    item: { type },
    collect: monitor => ({
      isDragging: !!monitor.isDragging()
    })
  });
  return (
    <div
      ref={drag}
      style={{
        opacity: isDragging ? 0.5 : 1
      }}
    >
      <Icon type={type} />
      <span unselectable="on">{children}</span>
    </div>
  );
};

export default DraggableItem;
