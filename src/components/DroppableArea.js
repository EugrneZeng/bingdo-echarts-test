import React from "react";
import { useDrop } from "react-dnd";
import { chartTypes } from "../utils/constants";
import { useCharts } from "../contexts/ChartsContext";

const DroppableArea = ({ children, height }) => {
  const { addChart } = useCharts();
  const [{ isOver }, drop] = useDrop({
    accept: [
      chartTypes.AREACHART,
      chartTypes.BARCHART,
      chartTypes.LINECHART,
      chartTypes.PIECHART
    ],
    drop: prop => addChart(prop.type),
    collect: monitor => ({
      isOver: !!monitor.isOver()
    })
  });
  const floatingStyle = {
    position: "absolute",
    top: 0,
    left: 0,
    height: "100%",
    width: "100%",
    zIndex: 1,
    opacity: 0.5,
    backgroundColor: "yellow"
  };
  return (
    <div ref={drop} style={{ position: "relative", width: "100%", height }}>
      {children}
      {isOver && <div style={floatingStyle} />}
    </div>
  );
};

export default DroppableArea;
