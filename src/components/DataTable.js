import React, { useState } from "react";
import { Table } from "antd";
import { useDataSource } from "../contexts/DataSourceContext";
import { useCharts } from "../contexts/ChartsContext";
import { MONTHES } from "../utils/constants";
import { flatten } from "../utils/carDataFormatter";

const getColumnChildren = month => [
  {
    title: "Count",
    dataIndex: `${month}-count`,
    width: "2.8%",
    key: `${month}-count`
  },
  {
    title: "Price",
    dataIndex: `${month}-price`,
    width: "4.8%",
    key: `${month}-price`,
    render: text => text && <span>{`$${text}`}</span>
  }
];
const columns = [
  {
    title: "Model",
    dataIndex: "name",
    key: "name"
  }
].concat(
  MONTHES.map(month => ({
    title: month,
    children: getColumnChildren(month)
  }))
);

const getChildrenData = data => {
  if (data.children) {
    return data.children.map(getChildrenData);
  } else {
    return data;
  }
};

const DataTable = () => {
  const { fileJson } = useDataSource();
  const { setChartData } = useCharts();

  const dataSource = fileJson ? fileJson.map(flatten) : [];
  const dataWithFields = dataSource.length
    ? dataSource
      .reduce((acc, ele) => acc.concat(getChildrenData(ele)), [])
      .flat()
    : [];

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  if (!fileJson || fileJson.length === 0) return null;
  const rowSelection = {
    selectedRowKeys,
    onChange: setSelectedRowKeys,
    onSelect: (record, selected, selectedRows, nativeEvent) => {
      const keys = selectedRows.map(row => row.key);
      setChartData(
        dataWithFields.filter(ele =>
          (keys.length ? keys : rowSelection.selectedRowKeys).some(
            key => key === ele.key
          )
        )
      );
    }
  };
  return (
    <Table
      size="small"
      pagination={false}
      rowSelection={rowSelection}
      bordered
      scroll={{ x: 4000, y: 400 }}
      defaultExpandAllRows
      columns={columns}
      dataSource={dataSource}
    />
  );
};

export default DataTable;
