import React, { useContext } from "react";

export const ChartsContext = React.createContext({});
export const useCharts = () => useContext(ChartsContext);
const chartReducer = (state, action) => {
  switch (action.type) {
    case "chart":
      return state.existingCharts.some(chart => chart === action.payload) ? state : {
        ...state,
        existingCharts: [...state.existingCharts].concat([action.payload])
      };
    case "chartData":
      return {
        ...state,
        chartData: action.payload
      };
    default:
      return state;
  }
};
export const ChartsProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(chartReducer, {
    existingCharts: [],
    chartData: []
  });
  return (
    <ChartsContext.Provider
      value={{
        ...state,
        setChartData: data => {
          dispatch({
            type: "chartData",
            payload: data
          });
        },
        addChart: chartType => {
          dispatch({
            type: "chart",
            payload: chartType
          });
        }
      }}
    >
      {children}
    </ChartsContext.Provider>
  );
};
