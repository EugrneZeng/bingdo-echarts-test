import React, { useContext } from "react";
import xlsxReader from "../utils/xlsxReader";
import { formatData } from "../utils/carDataFormatter";

export const DataSourceContext = React.createContext({});
export const useDataSource = () => useContext(DataSourceContext);

const SET_DATA_SOURCE = "setDataSource";
const dataSourceReducer = (state, action) => {
  switch (action.type) {
    case SET_DATA_SOURCE:
      return action.payload;
    default:
      throw new Error();
  }
};

export const DataSourceProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(dataSourceReducer, null);
  return (
    <DataSourceContext.Provider
      value={{
        fileJson: state,
        convertFileJson: async file => {
          try {
            const fileJson = await xlsxReader(file);
            dispatch({
              type: SET_DATA_SOURCE,
              payload: formatData(fileJson)
            });
          } catch (error) {
            console.error(error);
            dispatch({
              type: SET_DATA_SOURCE,
              payload: null
            });
          }
        }
      }}
    >
      {children}
    </DataSourceContext.Provider>
  );
};
