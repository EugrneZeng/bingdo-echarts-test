export const MONTHES = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
];
export const chartTypes = {
  LINECHART: "line-chart",
  AREACHART: "area-chart",
  BARCHART: "bar-chart",
  PIECHART: "pie-chart"
};

var labelOption = {
    show: true,
    rotate: 90,
    align: 'left',
    verticalAlign: 'middle',
    position: 'insideBottom',
    distance: 15,
    formatter: '{c}  {name|{a}}',
    fontSize: 12,
    rich: {
        name: {
            textBorderColor: '#fff'
        }
    }
};

export const chartOptions = {
    "line-chart": {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross'
            }
        },
        legend: {
            data:['Price', 'Count']
        },
        xAxis: {
            type: 'category',
            data: MONTHES
        },
        yAxis: [
            {
            type: 'value'
        },
        {
            type: 'value'
        }],
        series: [{
            data: null,
            type: 'line',
            name: 'Price',
        },
        {
            data: null,
            type: 'line',
            name: 'Count',
            yAxisIndex: 1
        }]
    },
    "area-chart": {},
    "bar-chart": {
        color: ['#003366', '#006699', '#4cabce', '#e5323e'],
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            data: ['Forest', 'Steppe', 'Desert', 'Wetland']
        },
        toolbox: {
            show: true,
            orient: 'vertical',
            left: 'right',
            top: 'center',
            feature: {
                mark: {show: true},
                dataView: {show: true, readOnly: false},
                magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        xAxis: [
            {
                type: 'category',
                axisTick: {show: false},
                data: ['2012', '2013', '2014', '2015', '2016']
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: 'Forest',
                type: 'bar',
                barGap: 0,
                label: labelOption,
                data: null
            }
        ]
    },
    "pie-chart": {
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b}: {c} ({d}%)'
        },
        legend: {
            orient: 'vertical',
            left: 10,
            data: null
        },
        series: [
            {
                name: 'Prices',
                type: 'pie',
                radius: ['50%', '70%'],
                avoidLabelOverlap: false,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '15',
                            fontWeight: 'bold'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data: null
            }
        ]
    }
}
