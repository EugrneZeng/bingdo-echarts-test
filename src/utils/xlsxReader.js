const xlsx = require('xlsx');


const xlsxReader = async excelFile => {
    if(!excelFile.type){
        throw new Error("Excel file isn't available");
    }
    const excelBuffer = await excelFile.arrayBuffer();
    const workbook = xlsx.read(excelBuffer,{
        type:'buffer',
        cellHTML:false,
    });
    var first_worksheet = workbook.Sheets[workbook.SheetNames[0]];
    return xlsx.utils.sheet_to_json(first_worksheet);
}

export default xlsxReader;
