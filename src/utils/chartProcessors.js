
import { MONTHES } from "./constants";

//{brand: "", type: "", name: "" }
//const isSame = key => dataArray.every(ele => ele[key] === dataArray[0][key]);
export const lineChartProssessor = dataArray =>
    dataArray.reduce(
        (accumulator, element) => {
            MONTHES.forEach((month, index) => {
                accumulator.Count[index] += element[`${month}-count`] || 0;
                accumulator.Price[index] += element[`${month}-price`] || 0;
            });
            return accumulator;
        },
        { Count: MONTHES.map(() => 0), Price: MONTHES.map(() => 0) }
    );

export const barChartProssessor = (dataArray, level = 0) => dataArray.reduce(
    (accumulator, element) => {
        MONTHES.forEach((month, index) => {
            accumulator[element.parentNames[level]][index] += element[`${month}-price`] || 0;
        });
        return accumulator;
    },
    dataArray.reduce(
        (accumulator, element) => {
            const parentName = element.parentNames[level];
            accumulator[parentName] = accumulator[parentName] || MONTHES.map(() => 0);
            return accumulator;
        },
        {}
    )
);

export const pieChartProssessor = (dataArray, level = 0) => dataArray.reduce(
    (accumulator, element) => {
        MONTHES.forEach((month, index) => {
            accumulator[element.parentNames[level]] += element[`${month}-price`] || 0;
        });
        return accumulator;
    },
    dataArray.reduce(
        (accumulator, element) => {
            const parentName = element.parentNames[level];
            accumulator[parentName] = accumulator[parentName] || 0;
            return accumulator;
        },
        {}
    )
);