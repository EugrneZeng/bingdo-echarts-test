import { MONTHES } from "./constants";

const parseToJson = ([key, value]) =>
  (index => value instanceof Map
    ? {
      name: key,
      key: index,
      children: [...value].map(parseToJson)
    }
    : {
      month: key,
      key: index,
      ...value
    })(Math.random().toString());

const flattenReducer = (flatted, element) => {
  const count = `${element.month}-count`;
  const price = `${element.month}-price`;
  flatted[count] = element.count;
  flatted[price] = element.price;
  flatted["key"] = element.key;
  flatted["parentNames"] = element.parentNames;
  return flatted;
};

export const flatten = ({ name, key, parentNames=[], children }) =>
  children.some(element => !!element.name)
    ? {
      name,
      key,
      children: children.map(child => ({ ...child, parentNames: parentNames.concat([name]) })).map(flatten)
    }
    : {
      name,
      ...children.map(child => ({ ...child, parentNames: parentNames.concat([name]) })).reduce(flattenReducer, {})
    };

export const formatData = data => {
  const dataIterator = data.reduce((accumulator, element) => {
    const sellDate = new Date((element.DATE - 1) * 24 * 3600000 + 1);
    const month = MONTHES[sellDate.getMonth()];
    const brand = accumulator.get(element.BRAND) || new Map();
    accumulator.set(element.BRAND, brand);
    const type = brand.get(element.TYPE) || new Map();
    brand.set(element.TYPE, type);
    const name = type.get(element.NAME) || new Map();
    type.set(element.NAME, name);
    const monthSell = name.get(month) || { count: 1, price: element.PRICE };
    name.set(month, monthSell);
    monthSell.count += 1;
    return accumulator;
  }, new Map());
  return [...dataIterator].map(parseToJson);
};
