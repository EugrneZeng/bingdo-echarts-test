import React from "react";
import Uploader from "./components/Uploader";
import DataTable from "./components/DataTable";
import SideMenu from "./components/SideMenu";
import DroppableArea from "./components/DroppableArea";
import ChartArea from "./components/ChartArea";
import { DataSourceProvider } from "./contexts/DataSourceContext";
import { ChartsProvider } from "./contexts/ChartsContext";
import { DndProvider } from "react-dnd";
import "./css/App.css";
import { Layout } from "antd";
import Backend from "react-dnd-html5-backend";

const { Header, Sider, Content } = Layout;

function App() {
  return (
    <Layout style={{ height: "100%" }}>
      <DndProvider backend={Backend}>
        <Sider style={{ height: "100%" }}>
          <SideMenu />
        </Sider>
        <Layout>
          <DataSourceProvider>
            <Header>
              <Uploader />
            </Header>
            <Content>
              <ChartsProvider>
                <DataTable />
                <DroppableArea height={300}>
                  <ChartArea />
                </DroppableArea>
              </ChartsProvider>
            </Content>
          </DataSourceProvider>
        </Layout>
      </DndProvider>
    </Layout>
  );
}

export default App;
